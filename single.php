<?php
/**
 * The Template for displaying all single posts
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

  <?php get_template_part('templates/content', 'single'); ?>

<?php endwhile; ?>
