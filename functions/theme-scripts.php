<?php
//
// Actions & Filters
//
  add_action( 'wp_enqueue_scripts', 'ransom_script_enqueuer' );

  /*
  Custom Post Types - include custom post types and taxonimies here
  e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
  */

//
// Scripts
//
  /** Add scripts via wp_head() */
  function ransom_script_enqueuer() {
    wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
    wp_enqueue_script( 'site' );

    wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
  }

//
// Read More Link
//
  add_filter( 'the_content_more_link', 'modify_read_more_link' );
  function modify_read_more_link() {
  return '<br><a class="more-link" href="' . get_permalink() . '">read more &rarr;</a>';
  }

?>
