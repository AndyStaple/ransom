
	<footer class="site-footer" role="contentinfo">
		<div class="site-info">
			<p>&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?>. All rights reserved.
			<span class="sep"> | </span>
			Built with Ransom by <a href="http://www.staplewebdesign.com/" rel="designer">Staple Web Design</a></p>
		</div>
	</footer>

</div><!-- .page.site -->

<script src="<?php bloginfo('template_url'); ?>/js/modernizr.custom.js"></script>

<?php wp_footer(); ?>

</body>
</html>
