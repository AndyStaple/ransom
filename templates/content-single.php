    <article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">

      <header>
        <h1 class="entry-title" itemprop="headline"><?php the_title(); ?></h1>
        <?php get_template_part('templates/entry-meta'); ?>
      </header>

      <div class="entry-content" itemprop="articleBody">
        <?php the_content(); ?>
      </div>

      <footer>
        <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>

        <div class="h" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
          <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
            <img src="" alt=""/>
            <meta itemprop="url" content="">
            <meta itemprop="width" content="400">
            <meta itemprop="height" content="120">
          </div>
          <meta itemprop="name" content="<?php $blog_title = get_bloginfo( 'name' ); ?>">
        </div>

      </footer>

    </article>
