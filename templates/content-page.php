    	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    		<h2 class="entry-title page-title"><?php the_title(); ?></h2>
    		<?php the_content(); ?>
        
    	</article>
