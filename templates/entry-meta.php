        <div class="entry-meta">
          <time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate  itemprop="datePublished" content="<?php the_time( 'Y-m-d' ); ?>">
            <?php the_date( 'M d, Y' ); ?>
          </time>
        </div>
