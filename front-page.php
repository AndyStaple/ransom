<?php
/**
 * The Front-Page Template, that will load on the Homepage
 */
?>

  <div class="article_list">
    <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part('templates/content', get_post_format()); ?>
    <?php endwhile; ?>
  </div>

  <?php if ($wp_query->max_num_pages > 1) : ?>
    <nav class="post-nav">
      <ul class="pager">
        <li class="previous"><?php next_posts_link(__('&larr; Older posts')); ?></li>
        <li class="next"><?php previous_posts_link(__('Newer posts &rarr;')); ?></li>
      </ul>
    </nav>
  <?php endif; ?>
