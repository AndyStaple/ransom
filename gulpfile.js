// ===================================================
// Setting
// ===================================================

// NODE ENV FLAGS SET VIA COMMAND LINE
// BEFORE RUNNING GULP FROM CLI
// export NODE_ENV=production
// export NODE_ENV=development

var gulp            = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    $               = gulpLoadPlugins({}),
    plumber         = require('gulp-plumber'),
    bourbon         = require('node-bourbon').includePaths,
    neat            = require('node-neat').includePaths,
    pngquant        = require('imagemin-pngquant');

$.exec   = require('child_process').exec;
$.fs     = require('fs');


// ===================================================
// Configuring
// ===================================================

var assetdir = {
  site: '.',
  dist: 'dist',
  js: 'js',
  css: '.',
  sass: 'scss',
  images: 'img'
};

var paths = {
  site: assetdir.site,
  dist: assetdir.dist,
  js: assetdir.site + '/' + assetdir.js,
  css: assetdir.site + '/' + assetdir.css,
  sass: assetdir.site + '/' + assetdir.sass,
  images: assetdir.site + '/' + assetdir.images,
};

var fileglob = {
  html: paths.site + '/*.html',
  css: paths.css + '/*.css',
  sass: paths.sass + '/**/*.scss',
  js: paths.js + '/src/**/*.js',
  jslibs : paths.js + '/lib/**/*.js'
};


// ===================================================
// Styling
// ===================================================

gulp.task('styles', function() {
  return gulp.src(fileglob.sass)
    .pipe($.if(
      process.env.NODE_ENV !== 'production',
      $.sourcemaps.init()
    ))
    .pipe(plumber())
    .pipe($.sass({
      includePaths: ['styles'].concat(neat),
      outputStyle: $.if(process.env.NODE_ENV !== 'production', 'expanded', 'compressed')
    }))
    .pipe($.autoprefixer({
      browsers: ['last 3 versions', 'IE 9'],
      cascade: false
    }))
    .pipe($.if(process.env.NODE_ENV !== 'production', $.sourcemaps.write('maps')))
    .pipe(gulp.dest(paths.css))
    .pipe($.livereload());
});


// ===================================================
// Optimizing
// ===================================================

gulp.task('imgmin', function() {
  var stream = gulp.src(paths.images + '/*.{jpg,png,jpeg,svg}')
    .pipe($.imagemin({
        progressive: true,
        svgoPlugins: [{
          removeViewBox: false
        }],
        use: [ pngquant() ]
    }))
    .pipe(gulp.dest(paths.images + '/' + path.dist));

  return stream;
});


gulp.task('svgstore', function() {
  var stream = gulp.src(paths.images + '/svgsprite/*.svg')
    .pipe($.svgmin({
      plugins: [{
        removeDoctype: true
      },
      {
        removeComments: true
      }]
    }))
    .pipe($.svgstore({
      inlineSvg: true
    }))
    .pipe($.cheerio({
      run: function($) {
        $('svg').attr('style', 'display:none');
      },
      parserOptions: {
        //https://github.com/cheeriojs/cheerio#loading
        //https://github.com/fb55/htmlparser2/wiki/Parser-options#option-xmlmode
        xmlMode: true
      }
    }))
    .pipe(gulp.dest(paths.images));

  return stream;
});


// ===================================================
// Watching
// ===================================================

gulp.task('watch',function() {
  gulp.watch(fileglob.sass, ['styles']);
  $.livereload.listen();
});


// ===================================================
// Tasking
// ===================================================

gulp.task('default', ['styles', 'watch']);
gulp.task('compressor', ['imgmin']);
gulp.task('svgsprite', ['svgstore']);
gulp.task('build', ['styles']);
